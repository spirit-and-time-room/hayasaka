# アプリの動かし方

**以下は開発する人向けの初回のみの設定**

```
VBoxManage sharedfolder add boot2docker-vm -name hysk-spirit-and-time -hostpath $HOME/Sites/php_apps/hayasaka/
```


### docker-composeでコンテナの起動

```
$ docker-compose up --build --force-recreate -d
```

## 動作環境

- php7.1系
- nginx
- php-fpm


