<?php

defined("DEBUG") || define("DEBUG", true);


if(DEBUG) {
  ini_set("display_errors", "On");
  ini_set("html_errors", "On");
}

spl_autoload_register(function($className){
  if(!class_exists($className)) {
    require_once __DIR__. "/vendor/". str_replace("\\", "/", $className).".php";
  }
});

