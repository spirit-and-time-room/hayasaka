<?php

namespace Hysk;

/**
 * 投稿データ一覧の表示
 */
require "bootstrap.php";

/**
 * ログインしていない場合はログイン画面へ遷移する
 */
if (!Auth::isAuthorized()) {
  Http\Response::redirectToLoginUrl();
}




