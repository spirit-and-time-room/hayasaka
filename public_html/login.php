<?php
namespace Hysk;

require_once "bootstrap.php";

if(Auth::isAuthorized()) {
  Http\Response::redirectToDashboardUrl();
}

$email = $password = null;
$messages = array();

if(isset($_POST["email"])) {
  $email = $_POST["email"];
}
if(isset($_POST["password"])) {
  $password = $_POST["password"];
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if(Auth::authenticate($email, $password)) {
    Http\Response::redirectToDashboardUrl();
  }
  else {
    $message = "ログインできませんでした。";
  }
}


require_once "views/login.html";

