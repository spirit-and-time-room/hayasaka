<?php

namespace Hysk;

use Hysk\Session;
use Hysk\Db;

class Auth {

  /**
   * 認証済みかどうかをチェックする
   * 認証済みの場合、trueを返す
   *
   * @return boolean
   */
  public static function isAuthorized() {
    $session = Session::getInstance();
  }

  /**
   * ユーザーの認証をDBアダプターを使って検証する
   */
  public static function authenticate($user, $pw) {
    // DBのアダプターを使って認証する
    $db = Db::getInstance();
    $db->findUser($user, $pw);
  }
}

