<?php

namespace Hysk;

use Hysk\Config;

class Db {
  /**
   * @Db
   */
  protected static $db;

  /**
   * SQLite$B$N(BDB$B%U%!%$%k%Q%9(B
   */
  protected static $db_file;

  /**
   * @PDO
   */
  protected $adapter;

  public static function getInstance() {
    if (!static::$db) {
      static::setDatabaseFile(realpath(__DIR__. "/../spirit_time.db"));
      static::$db = new self();
    }
    return static::$db;
  }

  public static function setDatabaseFile($file) {
    return static::$db_file = $file;
  }

  /**
   * $B%G!<%?%Y!<%9%U%!%$%k$r<hF@(B
   */
  public static function getDatabaseFile() {
    return static::$db_file;
  }

  public function __construct() {
    $adapter = new \PDO("sqlite:". static::getDatabaseFile());
    $this->setAdapter($adapter);
  }

  public function setAdapter(\PDO $adapter) {
    $this->setAdapter = $adapter;
    return $this;
  }

  public function getAdapter() {
    return $this->setAdapter;
  }

  public function query($sql, $args = array()) {
    $adapter = $this->getAdapter();
    $stmt = $adapter->prepare($sql);

    if ($stmt === false) {
      throw new \PDOException("PDO statement undefined.");
    }

    foreach($args as $placeholder => $value) {
      $stmt->bindParam($placeholder, $value);
    }
    return $stmt->execute();
  }

  /**
   * $B%m%0%$%s%f!<%6!<$rC5$9(B
   * $BB8:_$7$J$$>l9g$ONc30$rEj$2$k(B
   *
   * @param string $email
   * @param string $pass
   * @throw \PDO\Exception
   */
  public function findUser($email, $pass) {
    $sql = 'select * form users where :email = ? and :password = ?';
    $row = $this->query($sql, array(
      ":email"    => $email,
      ":password" => $pass
    ));

    if (count($row) <= 0) {
      throw new \PDOException("User NotFound.");
    }
    return $row;
  }
}
