<?php
namespace Hysk;

class Html {

  /**
   * 文字列の出力を行う
   *
   * @param $text   string
   * @param $escape boolean
   * @return string
   */
  public static function e($text, $escape = true) {
    if($escape === true) {
      $text = self::h($text);
    }

    echo $text;
  }

  /**
   * htmlspecialchars のエイリアス
   *
   * @param  $text    string
   * @return string
   */
  public static function h($text) {
    return htmlspecialchars($text, ENT_QUOTES);
  }
}


