<?php

namespace Hysk\Http;

class Response {
  protected $statusCodes = [
    100 => 'Continue',
    101 => 'Switching Protocols',
    102 => 'Processing',
    200 => 'OK',
    201 => 'Created',
    202 => 'Accepted',
    203 => 'Non-Authoritative Information',
    204 => 'No Content',
    205 => 'Reset Content',
    206 => 'Partial Content',
    207 => 'Multi-status',
    208 => 'Already Reported',
    226 => 'IM used',
    300 => 'Multiple Choices',
    301 => 'Moved Permanently',
    302 => 'Found',
    303 => 'See Other',
    304 => 'Not Modified',
    305 => 'Use Proxy',
    306 => 'Switch Proxy',
    307 => 'Temporary Redirect',
    308 => 'Permanent Redirect',
    400 => 'Bad Request',
    401 => 'Unauthorized',
    402 => 'Payment Required',
    403 => 'Forbidden',
    404 => 'Not Found',
    405 => 'Method Not Allowed',
    406 => 'Not Acceptable',
    407 => 'Proxy Authentication Required',
    408 => 'Request Timeout',
    409 => 'Conflict',
    410 => 'Gone',
    411 => 'Length Required',
    412 => 'Precondition Failed',
    413 => 'Request Entity Too Large',
    414 => 'Request-URI Too Large',
    415 => 'Unsupported Media Type',
    416 => 'Requested range not satisfiable',
    417 => 'Expectation Failed',
    418 => 'I\'m a teapot',
    421 => 'Misdirected Request',
    422 => 'Unprocessable Entity',
    423 => 'Locked',
    424 => 'Failed Dependency',
    425 => 'Unordered Collection',
    426 => 'Upgrade Required',
    428 => 'Precondition Required',
    429 => 'Too Many Requests',
    431 => 'Request Header Fields Too Large',
    444 => 'Connection Closed Without Response',
    451 => 'Unavailable For Legal Reasons',
    499 => 'Client Closed Request',
    500 => 'Internal Server Error',
    501 => 'Not Implemented',
    502 => 'Bad Gateway',
    503 => 'Service Unavailable',
    504 => 'Gateway Timeout',
    505 => 'Unsupported Version',
    506 => 'Variant Also Negotiates',
    507 => 'Insufficient Storage',
    508 => 'Loop Detected',
    510 => 'Not Extended',
    511 => 'Network Authentication Required',
    599 => 'Network Connect Timeout Error',
  ];

  public static function getInstance() {
    static $instance;
    if(!($instance instanceof Request)) {
      $instance = new static();
    }

    return $instance;
  }

  /**
   * リダイレクト処理を行う
   *
   * @param $uri    stirng 
   * @param $status int
   * @param $exit   boolean
   * @return boolean
   */
  public static function redirect($uri, $status = 304, $exit = true) {
    $instance = self::getInstance();
    $msg = $instance->getStatusMessage($status);
    header("HTTP/1.0 ${status} ${msg}");
    header("Location: ${uri}");
    if($exit) {
      exit;
    }
    return true;
  }

  /**
   * ログイン画面へリダイレクトする
   *
   * @return boolean
   */
  public static function redirectToLoginUrl() {
    return self::redirect("/login.php", 303, true);
  }

  /**
   * ダッシュボード画面へリダイレクトする
   *
   * @return boolean
   */
  public static function reidrectToDashboardUrl() {
    return self::redirect("/index.php", 303, true);
  }

  public function getStatusMessage($status) {
    if(!array_key_exists($status, $this->statusCodes)) {
      throw new ResponseException("status code ${status} undefined.");
    }

    return $this->statusCodes[$status];
  }
}
