<?php

namespace Hysk;

class Session {
  const SESSION_DEFAULT_NAMESPACE = "default";
  const SESSION_STATE_STARTED = true;
  const SESSION_STATE_NOT_STARTED = false;

  protected $namespace = "default";

  protected $sessionState = self::SESSION_STATE_NOT_STARTED;

  private static $instance = null;

  /**
   * セッションインスタンスの取得
   *
   * @param $namespace   string  セッションの固有の名前空間
   * @return Session
   */
  public static function getInstance($namespace = "default") {
    if(!self::$instance) {
      self::$instance = new self($namespace);
      self::$instance->sessionStart();
    }
    return self::$instance;
  }

  public function __construct($namespace = "default") {
    $this->setNamespace($namespace);
  }

  public function __destruct() {
    $this->destroy();
  }

  public function __get($key) {
    if(array_key_exists($key, $_SESSION[$this->namespace])) {
      return null;
    }
    return $_SESSION[$this->namespace][$key];
  }

  public function __set($key, $value) {
    $_SESSION[$this->namespace][$key] = $value;
  }

  protected function setNamespace($ns) {
    if (empty($ns)) {
      $ns = Session::SESSION_DEFAULT_NAMESPACE;
    }
    $this->namespace = $ns;
    return $this;
  }

  /**
   * セッションを開始する
   *
   * @return boolean
   */
  protected function sessionStart() {
    if($this->sessionState == self::SESSION_STATE_NOT_STARTED) {
      $this->sessionState = session_start();
      $_SESSION[$this->namespace] = null;
    }
    return $this->sessionState;
  }

  /**
   * セッションの破棄
   *
   * @return boolean
   */
  protected function destroy() {
    if($this->sessionState == self::SESSION_STATE_STARTED) {
      $this->sessionState = !session_destroy();
      unset($_SESSION[$this->namespace]);
      return !$this->sessionState;
    }
    return false;
  }
}
